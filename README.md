# Typescript static website template

Nothing fancy happening here. A template that bundles typescript for use in a static site. No libraries. No css transpiling. Just Typescript for direct dom manipulation. The future of the frontend stack.

Not really. This is just for having a base template to make an environment to fuck around in.

## Instructions

1. Clone repo
2. Rename all the stuff in `package.json`
3. Develop with `npm run start`
4. Build for prod with `npm run build`. Your static site is in `dist`.

Code goes in src, all static assets go in public.

Use your own formatter or linter I don't care.
